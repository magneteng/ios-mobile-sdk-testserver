function start(response, postData) {
  console.log("Request handler 'start' was called.");

    response.writeHead(200, {"Content-Type": "text/plain"});
    response.write("Hello World");
    response.end();
}
function upload(response, postData) { 
	console.log("Request handler 'upload' was called."); 
	response.writeHead(200, {"Content-Type": "text/plain"}); 
	response.write("You've sent: " + postData); 
	response.end();
}
exports.start = start;
exports.upload = upload;